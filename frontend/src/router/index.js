import {createRouter, createWebHashHistory} from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "about" */ '../views/Home'),
        children: [
            {
                path: '/hosts',
                component: () => import(/*webpackChunkName: "hosts"*/ '../views/Hosts')
            },
            {
                path: '/port',
                component: () => import(/*webpackChunkName: "port"*/ '../views/PortSearch')
            },
            {
                path: '/json',
                component: () => import(/*webpackChunkName: "port"*/ '../views/Json')
            },
            {
                path: '/toText',
                component: () => import(/*webpackChunkName: "port"*/ '../views/ToText')
            }
        ]
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router
