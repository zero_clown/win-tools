/* Do not change, this code is generated from Golang structs */

export {};

export class ResData {
    data: any;
    error: string;

    static createFrom(source: any = {}) {
        return new ResData(source);
    }

    constructor(source: any = {}) {
        if ('string' === typeof source) source = JSON.parse(source);
        this.data = source["data"];
        this.error = source["error"];
    }
}


