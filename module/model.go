package module

type ResData struct {
	Data  interface{} `json:"data"`
	Error string      `json:"error"`
}
